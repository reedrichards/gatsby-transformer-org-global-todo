const { parse } = require('orga')

const isUndefined = x => x === undefined

const isTodo = x => !isUndefined(x.keyword)

const onlyTodo = (i) => {
   todos = !isUndefined(i.children) ? i.children.map(onlyTodo) : []
   isTodo(i) ? todos.push(i) : false
   return [].concat.apply([],todos)
   
}

async function onCreateNode({ node, loadNodeContent }) {
    // only org files
    if (node.extension !== `org`) {
      return
    }
    const content = await loadNodeContent(node)
    const parsedContent = parse(content)
    todoItems = [].concat.apply([], parsedContent.children.map(onlyTodo))
    console.log("todos")
    console.table(todoItems)
  }
  exports.onCreateNode = onCreateNode